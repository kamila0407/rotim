from PyQt4 import QtGui, QtCore
import sys, math, copy
from PyQt4.QtCore import QPointF, QTimer
import threading, time
from PyQt4.QtGui import *
width = 1680
height = 900


## class MainWindow - основной класс работы программы, в котором задается интерфейс и производится вся дальнейшая работа (обработка нажатия кнопок и вычисления)

class MainWindow(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)

        self.setGeometry(100,100,width,height)
        self.setWindowTitle('RETURN OF THE INCREDIBLE MACHINES')
       
        self.StartButton = QtGui.QPushButton('START',self)
        self.StartButton.setGeometry(QtCore.QRect(width*0.005,0.48*height,width*0.14,0.2*height))
        self.StartButton.clicked.connect(self.Start)
        
        self.QuitButton = QtGui.QPushButton('Quit',self)
        self.QuitButton.setGeometry(QtCore.QRect(width*0.025,0.8*height,width*0.1,0.15*height))
        self.QuitButton.clicked.connect(self.Quit)

        self.BentPipeButton = QtGui.QPushButton('BENT PIPE',self)
        self.BentPipeButton.setGeometry(QtCore.QRect(375,807,100,60))
        self.BentPipeButton.clicked.connect(self.Add_Bent_Pipe)
        
        self.StraightPipeButton = QtGui.QPushButton('STRAIGHT PIPE',self)
        self.StraightPipeButton.setGeometry(QtCore.QRect(675,807,100,60))
        self.StraightPipeButton.clicked.connect(self.Add_Straight_Pipe)
        

        self.LEVEL = 1
        self.SpeedPipeButton = QtGui.QPushButton('SPEED PIPE',self)
        self.SpeedPipeButton.setGeometry(QtCore.QRect(950,807,100,60))
        self.SpeedPipeButton.clicked.connect(self.Add_Speed_Pipe)
        self.SpeedPipeButton.setVisible(False)
            
        self.NUMBER_OF_ELEMENTS = 0
        self.MASS = []
        self.BENT_PIPE = 0
        self.STRAIGHT_PIPE = 0


        self.ADD_SPEED_PIPE = 0
        self.Q = 0
        self.k = 0

        self.X = 440
        self.Y = 23
        self.Vx = 0
        self.Vy = 0
        self.time = 0

        self.BALL_INS_STR = 0
        self.BALL_INS_BNT = 0
        self.BALL_INS_SPD = 0

        self.n = 0
        self.RED = 0
        
## Quit(self) - функция обработки события нажатия на кнопку Quit. (Программа завершает свою работу, окно закрывается)      
    def Quit(self):
        if(self.Y > 23):
            self.timer.stop()
        sys.exit()

## Paint_start_background(self,p) - функция, создающая задний фон окна и разделяющаая его на 3 части: (поле игры, поле выбора элементов и поле с кнопками старт и выход)
# @param p - бъект типа paintEvent

    def Paint_start_background(self,p):
        p.setPen(QtGui.QPen(QtGui.QColor(128,128,128),5))
        p.drawLine(0.15*width,0,width*0.15,height)
        p.drawLine(0.15*width,0.76*height,width,0.76*height)
        #верхний
        if(self.LEVEL == 1):
            p.setBrush(QtGui.QBrush(QtGui.QColor(250,250,210)))
        if(self.LEVEL == 2):
            p.setBrush(QtGui.QBrush(QtGui.QColor(204,235,197)))
        if(self.LEVEL == 3):
            p.setBrush(QtGui.QBrush(QtGui.QColor(154,205,50)))
        p.drawRect(0,0,0.15*width,height)
        #нижний
        if(self.LEVEL == 1):
            p.setBrush(QtGui.QBrush(QtGui.QColor(224,255,255)))
        if(self.LEVEL == 2):
            p.setBrush(QtGui.QBrush(QtGui.QColor(255,224,178)))
        if(self.LEVEL == 3):
            p.setBrush(QtGui.QBrush(QtGui.QColor(240,230,140)))
        p.drawRect(0.15*width,height,0.85*width,-0.24*height)
        #главный+боковой
        if(self.LEVEL == 1):
            p.setBrush(QtGui.QBrush(QtGui.QColor(255,240,245)))
        if(self.LEVEL == 2):
            p.setBrush(QtGui.QBrush(QtGui.QColor(255,249,196)))
        if(self.LEVEL == 3):
            p.setBrush(QtGui.QBrush(QtGui.QColor(175,238,238)))
        p.drawRect(0.15*width,0.76*height,0.85*width,-0.76*height)
        p.drawRect(1060, height, 630, -0.24*height)
        p.drawLine(1680,0.76*height, 1680, height)
        #линия
        if(self.LEVEL == 1):
            p.setPen(QtGui.QPen(QtGui.QColor(255,240,245),5))
        if(self.LEVEL == 2):
            p.setPen(QtGui.QPen(QtGui.QColor(255,249,196),5))
        if(self.LEVEL == 3):
            p.setPen(QtGui.QPen(QtGui.QColor(175,238,238),5))
        p.drawLine(1065, 0.76*height, width -5, 0.76*height)
        # счетчик
        if(self.LEVEL == 3):
            p.setPen(QtGui.QPen(QtGui.QColor(128,128,128),5))
            p.setBrush(QtGui.QBrush(QtGui.QColor(221,160,221)))
            p.drawRect(825,754,235,-70)
            p.setFont(QtGui.QFont('Fantasy', 20))
            p.setPen(QtGui.QPen(QtGui.QColor(75,0,130),5))
            p.drawText(QtCore.QRect(825,754,235,-70), QtCore.Qt.AlignCenter,'PIPES LEFT : ' + str(4 - self.NUMBER_OF_ELEMENTS))
            

## Begin(self,p) - функция, рисующая начальную расстановку труб в соответствии с уровнем игры
    def Begin(self,p):
        if(self.LEVEL == 1):                               
            self.Draw_Straight_Pipe(640, 163, 2, p)
            self.MASS.append([640, 163,'straight_pipe',2])
            self.Draw_Straight_Pipe(800,393, 2, p)
            self.MASS.append([800,393,'straight_pipe',2])
            self.Draw_Straight_Pipe(965, 643, 2, p)
            self.MASS.append([965, 643,'straight_pipe',2])
            self.NUMBER_OF_ELEMENTS+=3
            self.Q+=3
        if(self.LEVEL == 2):
            self.Draw_Bent_Pipe(500, 200, 1, p)
            self.MASS.append([500, 200,'bent_pipe',1])            
            self.Draw_Bent_Pipe(985,605, 4, p)
            self.MASS.append([985,605,'bent_pipe',4])
            self.Draw_Straight_Pipe(1390, 643, 2, p)
            self.MASS.append([1390, 643,'straight_pipe',2])
            self.NUMBER_OF_ELEMENTS+=3
            self.Q+=3
## Draw_Text(self,p) - функция, рисующая значок игры и уровень
# @param p - бъект типа paintEvent

    def Draw_Text(self,p):
        pen = (QtGui.QPen(QtGui.QColor(70,130,180),4))
        pen.setStyle(QtCore.Qt.DotLine)
        p.setPen(pen)
        if(self.LEVEL == 1):
            p.setBrush(QtGui.QBrush(QtGui.QColor(255,222,173)))
        if(self.LEVEL == 2):
            p.setBrush(QtGui.QBrush(QtGui.QColor(253,215,228)))
        if(self.LEVEL == 3):
            p.setBrush(QtGui.QBrush(QtGui.QColor(240,230,140)))
        p.drawEllipse(width*0.005,0.08*height,width*0.14,0.25*height)
        p.setPen(QtGui.QPen(QtGui.QColor(70,130,180),2))
        
        p.drawRect(width*0.045,0.35*height,width*0.06,0.05*height)
        self.text1 = 'RETURN\nOF THE\nINCREDIBLE\nMACHINES'
        self.text3 = 'Choose the element:'
        p.setPen(QtGui.QColor(25,25,112))
        p.setFont(QtGui.QFont('Fantasy', 20))
        p.drawText(QtCore.QRect(width*0.005,0.08*height,width*0.14,0.25*height), QtCore.Qt.AlignCenter, self.text1)
        p.setFont(QtGui.QFont('Cursive', 15))
        p.drawText(QtCore.QRect(width*0.045,0.35*height,width*0.06,0.05*height), QtCore.Qt.AlignCenter, ' level '+str(self.LEVEL))
        if(self.LEVEL == 1):
            p.setBrush(QtGui.QBrush(QtGui.QColor(250,198,204)))
        if(self.LEVEL == 2):
            p.setBrush(QtGui.QBrush(QtGui.QColor(251,233,231)))
        if(self.LEVEL ==3):
            p.setBrush(QtGui.QBrush(QtGui.QColor(255,160,122)))
        p.drawRect(width*0.152,0.758*height,width*0.155,-0.06*height)
        p.drawText(QtCore.QRect(width*0.152,0.758*height,width*0.155,-0.06*height),  QtCore.Qt.AlignCenter, self.text3)

## Draw_Basket(self, x, y, p) - функция рисования корзины для мяча
# @param x - x-координата центра корзины
# @param y - y-координата центра корзины
# @param p - бъект типа paintEvent
    def Draw_Basket(self, x, y, p):
        p.setPen(QtGui.QPen(QtGui.QColor(0,0,0),2))
        p.setBrush(QtGui.QBrush(QtGui.QColor(105,105,105)))
        p.drawPolygon(QPointF(int(x)-47.5, int(y)+50),
        QPointF(int(x)-75, int(y)-50),
        QPointF(int(x)+75, int(y)-50),
        QPointF(int(x)+47.5, int(y)+50))
        
## Draw_Ball(self, x, y, p) - функция рисования шара
# @param x - x-координата центра шара
# @param y - y-координата центра шара
# @param p - бъект типа paintEvent

    def Draw_Ball(self, x, y, p):
        p.setPen(QtGui.QPen(QtGui.QColor(0,0,0),4))
        p.setBrush(QtGui.QBrush(QtGui.QColor(105,105,105)))
        p.drawEllipse(int(x)-17,int(y)+17,34,-34)
        
## Draw_Bent_Pipe(self, x, y, p) - функция рисования изогнутой трубы
# @param x - x-координата центра стандартного положения трубы
# @param y - y-координата центра стандартного положения трубы
# @ param p - бъект типа paintEvent

    def Draw_Bent_Pipe(self, x, y, rotation, p):
        p.setPen(QtGui.QPen(QtGui.QColor(128,128,128),5))
        p.setBrush(QtGui.QBrush(QtGui.QColor(192,192,192)))
        if(rotation == 1):
            p.drawRect(int(x)-75,int(y)+75,75,-150)
            p.drawRect(int(x),int(y)-75,75,75)
            p.setPen(QtGui.QPen(QtGui.QColor(192,192,192),5))
            p.drawLine(int(x),int(y)-70,int(x),int(y)-5)
        if(rotation == 2):
            p.drawRect(int(x)-75,int(y),150,-75)
            p.drawRect(int(x),int(y),75,75)
            p.setPen(QtGui.QPen(QtGui.QColor(192,192,192),5))
            p.drawLine(int(x)+5,int(y),int(x)+70,int(y))
        if(rotation == 3):
            p.drawRect(int(x),int(y)+75,75,-150)
            p.drawRect(int(x)-75,int(y),75,75)
            p.setPen(QtGui.QPen(QtGui.QColor(192,192,192),5))
            p.drawLine(int(x),int(y)+5,int(x),int(y)+70)
        if(rotation == 4):
            p.drawRect(int(x)-75,int(y)+75,150,-75)
            p.drawRect(int(x)-75,int(y)-75,75,75)
            p.setPen(QtGui.QPen(QtGui.QColor(192,192,192),5))
            p.drawLine(int(x)-70,int(y),int(x)-5,int(y))
        

## Draw_Straight_Pipe(self, x, y, p) - функция рисования прямой трубы
# @param x - x-координата центра стандартного положения трубы
# @param y - y-координата центра стандартного положения трубы
# @param p - бъект типа paintEvent

    def Draw_Straight_Pipe(self, x, y, rotation, p):
        p.setPen(QtGui.QPen(QtGui.QColor(128,128,128),5))
        p.setBrush(QtGui.QBrush(QtGui.QColor(192,192,192)))
        if( rotation == 1):
            p.drawRect(int(x)-37.5,int(y)+75,75,-150)
        if( rotation == 2):
            p.drawRect(int(x)-75,int(y)+37.5,150,-75)

## Draw_Speed_Pipe(self, x, y, p) - функция рисования ускоряющей трубы
# @param x - x-координата центра стандартного положения трубы
# @param y - y-координата центра стандартного положения трубы
# @param p - бъект типа paintEvent

    def Draw_Speed_Pipe(self, x, y, rotation, p):
        p.setPen(QtGui.QPen(QtGui.QColor(128,128,128),5))
        p.setBrush(QtGui.QBrush(QtGui.QColor(255,255,0)))
        if( rotation == 1):
            p.drawRect(int(x)-30,int(y)+37.5,60,-75)
        if( rotation == 2):
            p.drawRect(int(x)-37.5,int(y)+30,75,-60)

## Add_Bent_Pipe(self) - функция обработки события нажатия кнопки BentPipeButton
        
    def Add_Bent_Pipe(self):
        self.BENT_PIPE = 1
        self.NUMBER_OF_ELEMENTS+=1
        self.STRAIGHT_PIPE = 0
        self.SPEED_PIPE = 0
        if(self.LEVEL == 3 and self.NUMBER_OF_ELEMENTS > 4):
            self.NUMBER_OF_ELEMENTS-=1
            QMessageBox.information(self, "ATTENTION!", "You have no pipes left!")
    
## Add_Straight_Pipe(self) - функция обработки события нажатия кнопки StraightPipeButton
        
    def Add_Straight_Pipe(self):
        self.STRAIGHT_PIPE = 1
        self.NUMBER_OF_ELEMENTS+=1
        self.BENT_PIPE = 0
        self.SPEED_PIPE = 0
        if(self.LEVEL == 3 and self.NUMBER_OF_ELEMENTS > 4):
            self.NUMBER_OF_ELEMENTS-=1
            QMessageBox.information(self, "ATTENTION!", "You have no pipes left!")

## Add_Speed_Pipe(self) - функция обработки события нажатия кнопки SpeedPipeButton
        
    def Add_Speed_Pipe(self):
        self.SPEED_PIPE = 1
        self.NUMBER_OF_ELEMENTS+=1
        self.BENT_PIPE = 0
        self.STRAIGHT_PIPE = 0
        if(self.LEVEL == 3 and self.NUMBER_OF_ELEMENTS > 4):
            self.NUMBER_OF_ELEMENTS-=1
            QMessageBox.information(self, "ATTENTION!", "You have no pipes left!")
                
## Press_Location(self,x,y) - функция, проверяющая, совпадает ли место двойного нажатия на мышь с центром какого-нибудь элемента   
    def Press_Location(self,x,y):
        for i in range(self.NUMBER_OF_ELEMENTS):
            if(x <= self.MASS[i][0]+30 and x >= self.MASS[i][0]-30 and y >= self.MASS[i][1]-30 and y <= self.MASS[i][1]+30):
                self.k = i;
    
## mousePressEvent(self,q) - функция обработки события нажатия мыши на экран после нажатия кнопок StraightPipeButton или BentPipeButton
# @param p - бъект типа paintEvent       
    def mousePressEvent(self,q):
        if((self.STRAIGHT_PIPE==1 and self.BENT_PIPE==0 and self.SPEED_PIPE == 0) or (self.STRAIGHT_PIPE==0 and self.BENT_PIPE==1 and self.SPEED_PIPE == 0) or (self.SPEED_PIPE ==1 and self.STRAIGHT_PIPE == self.BENT_PIPE == 0)):
            self.Q +=1
            if self.BENT_PIPE == 1:
                self.MASS.append([int(q.x()),int(q.y()),'bent_pipe',1])
                self.BENT_PIPE = 0
                self.update()
            if self.STRAIGHT_PIPE == 1:
                self.MASS.append([int(q.x()),int(q.y()),'straight_pipe',1])
                self.STRAIGHT_PIPE = 0
                self.update()
            if self.SPEED_PIPE == 1:
                self.MASS.append([int(q.x()),int(q.y()),'speed_pipe',1])
                self.SPEED_PIPE = 0
                self.update()
                
## mouseDoubleClickEvent(self, q) - функция обработки события двойного нажатия мыши на экран для поворота элемента
# @param p - бъект типа paintEvent
    def mouseDoubleClickEvent(self, q):
        self.Press_Location(int(q.x()),int(q.y()))
        if (self.MASS[self.k][2] == 'bent_pipe'):
            self.MASS[self.k][3] +=1
            if (self.MASS[self.k][3] == 5):
                self.MASS[self.k][3] = 1
            self.update()
        if (self.MASS[self.k][2] == 'straight_pipe'):
            self.MASS[self.k][3] +=1
            if (self.MASS[self.k][3] == 3):
                self.MASS[self.k][3] = 1
            self.update()
        if (self.MASS[self.k][2] == 'speed_pipe'):
            self.MASS[self.k][3] +=1
            if (self.MASS[self.k][3] == 3):
                self.MASS[self.k][3] = 1
            self.update()

#############################################################################################################################################################################################
## Ball_Inside (self) - функция, определяющая:
#            1)находится ли шар в данный момент в какой-либо из труб
#            2)в какой именно трубе он находится

    def Ball_Inside (self):
        for i in range(self.NUMBER_OF_ELEMENTS):
            
            if (self.MASS[i][2] == 'straight_pipe' and self.MASS[i][3] == 1):
                if(self.X+15 <= self.MASS[i][0]+35 and self.X-15 >= self.MASS[i][0]-35 and self.Y + 15 <= self.MASS[i][1]-65 and self.Y + 15 >= self.MASS[i][1]-75  and self.BALL_INS_STR==self.BALL_INS_STR==self.BALL_INS_SPD==0):
                    print("BALL INSIDE STRAIGHT PIPE 1")
                    self.BALL_INS_STR = 1
                    self.n = i

            if (self.MASS[i][2] == 'bent_pipe' and self.MASS[i][3] == 3):
                if(self.X+15 <= self.MASS[i][0]+75 and self.X-15 >= self.MASS[i][0] and self.Y + 15 <= self.MASS[i][1]-15 and self.Y + 15 >= self.MASS[i][1]-75 and self.BALL_INS_BNT==self.BALL_INS_STR==self.BALL_INS_SPD==0):
                    print("BALL INSIDE BENT PIPE 3")
                    self.BALL_INS_BNT = 3
                    self.n = i
                        
            if (self.MASS[i][2] == 'bent_pipe' and self.MASS[i][3] == 4):
                if(self.X+15 <= self.MASS[i][0] and self.X-15 >= self.MASS[i][0]-75 and self.Y + 15 <= self.MASS[i][1]-15 and self.Y + 15 >= self.MASS[i][1]-75 and self.BALL_INS_BNT==self.BALL_INS_STR==self.BALL_INS_SPD==self.BALL_INS_SPD==0):
                    print("BALL INSIDE BENT PIPE 4")
                    self.BALL_INS_BNT = 4
                    self.n = i
            if(self.LEVEL > 1):
                if (self.MASS[i][2] == 'speed_pipe' and self.MASS[i][3] == 2):
                    if(self.X+15 <= self.MASS[i][0]+37 and self.X-15 >= self.MASS[i][0]-37 and self.Y + 15 <= self.MASS[i][1] and self.Y + 15 >= self.MASS[i][1]-30  and self.BALL_INS_STR==self.BALL_INS_STR==self.BALL_INS_SPD==0):
                        print("BALL INSIDE SPEED PIPE 1")
                        self.BALL_INS_SPD = 1
                        self.n = i

            if(self.Vx > 0):
                if (self.MASS[i][2] == 'straight_pipe' and self.MASS[i][3] == 2):
                    if(self.X+15 >= self.MASS[i][0] - 75 and self.X+15 <= self.MASS[i][0]- 45 and self.Y-15 >= self.MASS[i][1]-35 and self.Y+15 <= self.MASS[i][1]+35 and self.BALL_INS_STR==self.BALL_INS_STR==self.BALL_INS_SPD==0):
                        print("BALL INSIDE STRAIGHT PIPE 2.1")
                        self.BALL_INS_STR = 2.1
                        self.n = i
                if (self.MASS[i][2] == 'bent_pipe' and self.MASS[i][3] == 2):
                    if(self.X+15 <= self.MASS[i][0]-15  and self.X+15 >= self.MASS[i][0]-75 and self.Y + 15 <= self.MASS[i][1] and self.Y - 15 >= self.MASS[i][1]-75 and self.BALL_INS_BNT==self.BALL_INS_STR==self.BALL_INS_SPD==0):
                        print("BALL INSIDE BENT PIPE 2")
                        self.BALL_INS_BNT = 2
                        self.n = i
                if(self.LEVEL > 1):
                    if (self.MASS[i][2] == 'speed_pipe' and self.MASS[i][3] == 1):
                        if(self.X+15 >= self.MASS[i][0]-30 and self.X+15 <= self.MASS[i][0] and self.Y-15 >= self.MASS[i][1]-37 and self.Y+15 <= self.MASS[i][1]+37 and self.BALL_INS_STR==self.BALL_INS_BNT==self.BALL_INS_SPD==0):
                            print("BALL INSIDE SPEED PIPE 2.1")
                            self.BALL_INS_SPD = 2.1
                            self.n = i
            if(self.Vx < 0):
                if (self.MASS[i][2] == 'straight_pipe' and self.MASS[i][3] == 2):
                    if(self.X-15 >= self.MASS[i][0] + 55 and self.X-15 <= self.MASS[i][0] + 75 and self.Y-15 >= self.MASS[i][1]-35 and self.Y+15 <= self.MASS[i][1]+35 and self.BALL_INS_STR==self.BALL_INS_BNT==self.BALL_INS_SPD==0):
                        print("BALL INSIDE STRAIGHT PIPE 2.2")
                        self.BALL_INS_STR = 2.2
                        self.n = i
                if (self.MASS[i][2] == 'bent_pipe' and self.MASS[i][3] == 1):
                    if(self.X-15 <= self.MASS[i][0]+75 and self.X-15 >= self.MASS[i][0]+15 and self.Y + 15 <= self.MASS[i][1] and self.Y - 15 >= self.MASS[i][1]-75 and self.BALL_INS_BNT==self.BALL_INS_STR==self.BALL_INS_SPD==0):
                        print("BALL INSIDE BENT PIPE 1")
                        self.BALL_INS_BNT = 1
                        self.n = i
                if(self.LEVEL > 1):
                    if (self.MASS[i][2] == 'speed_pipe' and self.MASS[i][3] == 1):
                        if(self.X-15 >= self.MASS[i][0] and self.X-15 <= self.MASS[i][0] + 30 and self.Y-15 >= self.MASS[i][1]-37 and self.Y+15 <= self.MASS[i][1]+37 and self.BALL_INS_STR==self.BALL_INS_BNT==self.BALL_INS_SPD==0):
                            print("BALL INSIDE SPEED PIPE 2.2")
                            self.BALL_INS_SPD = 2.2
                            self.n = i
## Redirection(self) - функция, определяющая:
#            1)нужно ли шару поменять направление движения в зависимости от того, в какой трубе он находится
#            2)выкатился ли шар из трубы
    def Redirection(self):
        #1
        if(self.BALL_INS_STR == 1):
            self.RED = -1
            self.Vx = 0
        if (self.RED == -1 and self.Y >=self.MASS[self.n][1]+75):
            print("BALL OUTSIDE STRAIGHT PIPE 1")
            self.BALL_INS_STR = 0
            self.RED = 0
            self.n = 0
        #1 speed
        if(self.BALL_INS_SPD == 1):
            self.RED = -1.11
            self.Vx = 0
            self.Vy+=1
        if (self.RED == -1.11 and self.Y >=self.MASS[self.n][1]+37):
            print("BALL OUTSIDE SPEED PIPE 1")
            self.BALL_INS_SPD = 0
            self.RED = 0
            self.n = 0
        #2.1
        if(self.BALL_INS_STR == 2.1):
            self.RED = -2.1
            self.Vy = 0
        if (self.RED == -2.1 and self.X+15 >=self.MASS[self.n][0]+75):
            print("BALL OUTSIDE STRAIGHT PIPE 2.1")
            self.BALL_INS_STR = 0
            self.RED = 0
            self.n = 0
        #2.2
        if(self.BALL_INS_STR == 2.2):
            self.RED = -2.2
            self.Vy = 0
        if (self.RED == -2.2 and self.X <=self.MASS[self.n][0]-75):
            print("BALL OUTSIDE STRAIGHT PIPE 2.2")
            self.BALL_INS_STR = 0
            self.RED = 0
            self.n = 0
        #2.1 speed
        if(self.BALL_INS_SPD == 2.1):
            self.RED = -2.21
            self.Vy = 0
            self.Vx+=1
        if (self.RED == -2.21 and self.X+15 >=self.MASS[self.n][0]+30):
            print("BALL OUTSIDE SPEED PIPE 2.2")
            self.BALL_INS_SPD = 0
            self.RED = 0
            self.n = 0
        #2.2 speed
        if(self.BALL_INS_SPD == 2.2):
            self.RED = -2.22
            self.Vy = 0
            self.Vx-=1
        if (self.RED == -2.22 and self.X-15 <=self.MASS[self.n][0]-30):
            print("BALL OUTSIDE SPEED PIPE 2.2")
            self.BALL_INS_SPD = 0
            self.RED = 0
            self.n = 0
        # 1 bnt
        if(self.BALL_INS_BNT == 1 and self.RED == 0 ):
            if(self.MASS[self.n][1] - self.Y <= self.MASS[self.n][0]-self.X+3 and  self.MASS[self.n][1] - self.Y >= self.MASS[self.n][0]-self.X-3):
                print("FOUND 1")
                self.RED = 1
                self.Vy = self.Vx * (-1)
                self.Vx = 0
                self.BALL_INS_BNT = 1.001
        if(self.RED == 1 and self.Y >=self.MASS[self.n][1]+75):
                print("BALL OUTSIDE BENT PIPE 1")
                self.BALL_INS_BNT = 0
                self.RED = 0
                self.n = 0
        # 2 bnt
        if(self.BALL_INS_BNT == 2 and self.RED == 0 ):
            if(self.MASS[self.n][1] - self.Y <= self.X - self.MASS[self.n][0]+4 and  self.MASS[self.n][1] - self.Y >= self.X-self.MASS[self.n][0]-4):
                print("FOUND 2")
                self.RED = 2
                self.Vy = self.Vx
                self.Vx = 0
                self.BALL_INS_BNT = 2.002
        if(self.RED == 2 and self.Y >=self.MASS[self.n][1]+75):
                print("BALL OUTSIDE BENT PIPE 2")
                self.BALL_INS_BNT = 0
                self.RED = 0
                self.n = 0
        # 3
        if(self.BALL_INS_BNT == 3 and self.RED == 0 ):
            if(self.Y - self.MASS[self.n][1] <= self.X-self.MASS[self.n][0]+3 and self.Y - self.MASS[self.n][1] >= self.X-self.MASS[self.n][0]-3):
                print("FOUND 3")
                self.RED = 3
                self.Vx = self.Vy * (-1)
                self.Vy = 0
        if(self.RED == 3 and self.X <=self.MASS[self.n][0]-75):
                print("BALL OUTSIDE BENT PIPE 3")
                self.BALL_INS_BNT = 0
                self.RED = 0
                self.n = 0
        # 4
        if(self.BALL_INS_BNT == 4 and self.RED == 0 ):
            if(self.Y - self.MASS[self.n][1] <= self.MASS[self.n][0]-self.X+3 and self.Y - self.MASS[self.n][1] >= self.MASS[self.n][0] - self.X-3):
                print("FOUND 4")
                self.RED = 4
                self.Vx = self.Vy
                self.Vy = 0
        if(self.RED == 4 and self.X+15 >=self.MASS[self.n][0]+75):
                print("BALL OUTSIDE BENT PIPE 4")
                self.BALL_INS_BNT = 0
                self.RED = 0
                self.n = 0

## Finish(self) - функция, определяющая, попал ли мяч в корзину, и прерывающая таймер, если попал
    def Finish(self):
        if(self.LEVEL == 1 ):
            if(self.X+15 <= 1350 + 75 and self.X-15 >= 1350 - 75 and self.Y-15 >= 845 and self.Y + 15 <= 845 + 45):
                print("BALL INSIDE BASKET!!!!!")
                self.timer.stop()
                self.Level_up()
        if(self.LEVEL == 2 ):
            if(self.X+15 <= 1590 + 75 and self.X-15 >= 1590 - 75 and self.Y-15 >= 845 and self.Y + 15 <= 845 + 45):
                print("BALL INSIDE BASKET!!!!!")
                self.timer.stop()
                self.Level_up()
        if(self.LEVEL == 3):
            if(self.X+15 <= 1590 + 75 and self.X-15 >= 1590 - 75 and self.Y-15 >= 845 and self.Y + 15 <= 845 + 45):
                print("BALL INSIDE BASKET!!!!!")
                self.timer.stop()
                self.End_of_game()
        if(self.Y - 15 >= height):
            print("BALL OUT!!!")
            self.timer.stop()
            self.Play_again()
            
## Level_up(self) - функция перехода на следующий уровень             
    def Level_up(self): 
        choice = QtGui.QMessageBox.question(self,'CONGRATULATIONS',"You win! Go to the next level?",
                                            QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,  QMessageBox.Yes)
        if choice == QtGui.QMessageBox.Yes:
            self.LEVEL+=1
            self.SpeedPipeButton.setVisible(True)
            self.NUMBER_OF_ELEMENTS = 0
            self.MASS = []
            self.Q = 0
            self.k = 0
            self.RED = 0
            self.BALL_INS_BNT = 0
            self.BALL_INS_STR = 0
            self.BALL_INS_SPD = 0
            if(self.LEVEL == 2):
                self.X = 933
                self.Y = 23
                self.Vx = 0
                self.Vy = 0
            if(self.LEVEL == 3):
                self.X = 320
                self.Y = 23
                self.Vx = 0
                self.Vy = 0
                QMessageBox.information(self, "ATTENTION!", "You can only use 4 pipes for this level!")                
        else:
            pass        
        
## Play_again(self) - функция, информирующая о проигрыше   
    def Play_again(self):
        QMessageBox.information(self, "SORRY", "You lost. Try again!")
        self.NUMBER_OF_ELEMENTS = 0
        self.MASS = []
        self.Q = 0
        self.k = 0
        self.RED = 0
        
        self.BALL_INS_BNT = 0
        self.BALL_INS_STR = 0
        if(self.LEVEL == 1):
            self.X = 440
            self.Y = 23
            self.Vx = 0
            self.Vy = 0
        if(self.LEVEL == 2):
            self.BALL_INS_SPD = 0
            self.X = 933
            self.Y = 23
            self.Vx = 0
            self.Vy = 0
        if(self.LEVEL == 3):
            self.BALL_INS_SPD = 0
            self.X = 320
            self.Y = 23
            self.Vx = 0
            self.Vy = 0

## End_of_game(self) - функция, информирующая об успешном прохождении всей игры
    def End_of_game(self):
        QMessageBox.information(self, "    VICTORY    ","  ! ! ! ! ! VICTORY ! ! ! ! !  ")
#############################################################################################################################################################################################

## X_time(self) - функция зависимости координаты x шара от времени
    def X_time(self) :
        if(self.RED != (-1) and self.RED != (-1.11)):
            self.X = (self.X + self.Vx * self.time)
## Y_time(self) - функция зависимости координаты y шара от времени
    def Y_time (self) :
        if(self.RED != 3 and self.RED != 4 and self.RED != -2.1 and self.RED != 2.2 and self.BALL_INS_BNT != 1 and self.BALL_INS_BNT != 2):
            self.Y = self.Y + self.Vy * self.time + 10 * (self.time*self.time)/2
## Xy_time(self) - функция зависимости x-координаты скорости шара от времени
    def Vx_time (self) :
        self.Vx = self.Vx
## Vy_time(self) - функция зависимости y-координаты скорости шара от времени
    def Vy_time (self) :
        if(self.RED != -2.1 and self.RED != 2.2):
            self.Vy = self.Vy + self.time * 10
                   
## Ball_Location(self) - функция, осуществляющая движение шара путем вызова функций-зависимостей и update 
    def Ball_Location( self ):
        self.Ball_Inside()
        self.Redirection()

        self.X_time()
        self.Y_time()
        self.Vx_time()
        self.Vy_time()
        self.Finish()
        self.update()
        
## Start(self) - функция обработки нажатия мыши на кнопку Start, в этой функции заводится таймер
    def Start(self):
        #self.time=0.05
        self.time=0.035
        self.Ball_Location()
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.Timer_Event)
        self.timer.start(10)
        
## Timer_Event(self) - функция, вызывающаяся каждый раз при срабатывании таймера
    def Timer_Event(self):
        self.Ball_Location()
        
#############################################################################################################################################################################################
            
## paintEvent(self,p) - функция рисования, вызывающая функции Paint_start_background(self,p) и Draw_Text(self,p)
# @param p - бъект типа paintEvent
    def paintEvent(self,p):
        p = QtGui.QPainter()
        p.begin(self)      
        self.Paint_start_background(p)
        self.Draw_Text(p)
        if(self.NUMBER_OF_ELEMENTS == 0):
            self.Paint_start_background(p)
            self.Draw_Text(p)
            self.Begin(p)  
        self.Draw_Bent_Pipe(360, 792, 1, p)
        self.Draw_Straight_Pipe(622.5, 792, 1, p)
        if(self.LEVEL > 1):
            self.Draw_Speed_Pipe(910, 829.5, 1, p)           

        if(self.LEVEL == 1):
            self.Draw_Ball(self.X,self.Y,p)
            self.Draw_Basket(1350,845,p)   
        if(self.LEVEL == 2):
            self.Draw_Ball(self.X,self.Y,p)
            self.Draw_Basket(1590,845,p)  
        if(self.LEVEL == 3):
            self.Draw_Ball(self.X,self.Y,p)
            self.Draw_Basket(1590,845,p)  
                  
        if(self.Q==self.NUMBER_OF_ELEMENTS and self.Q!=0):
            for i in range(self.NUMBER_OF_ELEMENTS):
                if self.MASS[i][2] == 'bent_pipe' :
                    self.Draw_Bent_Pipe(self.MASS[i][0],self.MASS[i][1], self.MASS[i][3], p)
                if self.MASS[i][2] == 'straight_pipe' :
                    self.Draw_Straight_Pipe(self.MASS[i][0],self.MASS[i][1], self.MASS[i][3], p)
                if self.MASS[i][2] == 'speed_pipe' :
                    self.Draw_Speed_Pipe(self.MASS[i][0],self.MASS[i][1], self.MASS[i][3], p)
                
        p.end

def main():
    app = QtGui.QApplication(sys.argv)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
       main()
 
